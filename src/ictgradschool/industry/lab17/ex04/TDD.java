package ictgradschool.industry.lab17.ex04;

import ictgradschool.Keyboard;

/**
 * Created by ljam763 on 15/05/2017.
 */
public class TDD {
    public static double ConvertionStringToDouble(String number){
        return Double.parseDouble(number);
    }
    public double rectangle(double length, double width)throws NegativeExpections{
        if (length < 0 || width <0){
            throw new NegativeExpections();
        }
        return length*width;
    }
    public double triangle(double radius) throws NegativeExpections{
        if (radius < 0){
            throw new NegativeExpections();
        }
        return Math.pow(radius,2)*Math.PI;
    }
    public int rounding(double results)throws NegativeExpections{
        return Math.round((float)triangle(results));
    }
    public int roundingRec (double length, double width)throws NegativeExpections{
        if (length < 0 || width <0){
            throw new NegativeExpections();
        }
        return Math.round((float)rectangle(length, width));
    }
    public boolean comparingSmall ( double one ,double two){
        return one < two ;
    }
    public int smaller (double one ,double two, double three)throws NegativeExpections{
        if (one<0 || two< 0 || three <0){
            throw new NegativeExpections();
        }
        int smaller = (rectangle(two, three)< triangle(one))? roundingRec(two,three): rounding(one);
        return smaller;
    }

    public static void main(String[] args) {
        TDD testing = new TDD();
        System.out.println("Welcome to Shape Area Calculator!");
        System.out.println();
        System.out.print("Enter the width of the rectangle: ");
        double number1 = ConvertionStringToDouble(Keyboard.readInput());
        System.out.print("Enter the length of the rectangle: ");
        double number2 = ConvertionStringToDouble(Keyboard.readInput());
        System.out.println();
        System.out.print("The radius of the circle is: ");
        System.out.println();
        double number3 = ConvertionStringToDouble(Keyboard.readInput());
        try {
            System.out.print("The smaller area is: "+ testing.smaller(number3, number1, number2));
        } catch (NegativeExpections negativeExpections) {
            negativeExpections.printStackTrace();
        }

    }
}
