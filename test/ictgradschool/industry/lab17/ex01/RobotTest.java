package ictgradschool.industry.lab17.ex01;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state
            assertEquals(1, myRobot.currentState().row);
        }
    }

    @Test
    public void testTurn(){
        Robot.RobotState currentState = myRobot.currentState();
        myRobot.turn();
        assertEquals(currentState.column, myRobot.column());
        assertEquals(currentState.row, myRobot.row());
        switch (myRobot.getDirection()){
            case East:
                assertEquals(Robot.Direction.South, myRobot.getDirection());
                break;
            case North:
                assertEquals(Robot.Direction.East, myRobot.getDirection());
                break;
            case South:
                assertEquals(Robot.Direction.West, myRobot.getDirection());
                break;
            case West:
                assertEquals(Robot.Direction.North, myRobot.getDirection());
                break;
        }
    }

    @Test
    public void moveOutTop() throws IllegalMoveException{
        try{
            for (int i = 0; i < Robot.GRID_SIZE ; i++) {
                myRobot.move();
                System.out.println(myRobot.row());
            }
            fail();
        }
        catch (IllegalMoveException e){
            assertEquals(1, myRobot.row());
        }
    }

    @Test
    public void moveinTop() throws IllegalMoveException{
        try{
            for (int i = 0; i < Robot.GRID_SIZE-1 ; i++) {
                myRobot.move();
                System.out.println(myRobot.row());
            }
            assertEquals(1, myRobot.row());
        }
        catch (IllegalMoveException e){
            fail();
        }
    }
    @Test
    public void moveOutDown() throws IllegalMoveException{
        try{
            myRobot.turn();
            myRobot.turn();
            for (int i = 0; i < Robot.GRID_SIZE ; i++) {
                myRobot.move();
                System.out.println(myRobot.row());
            }
            fail();
        }
        catch (IllegalMoveException e){
            assertEquals(10, myRobot.row());
        }
    }

    @Test
    public void moveinDown() throws IllegalMoveException{
        myRobot.turn();
        myRobot.turn();
        int row = myRobot.row();
        try{
            for (int i = 1; i >= row ; i++) {
                System.out.println(myRobot.row());
                myRobot.move();
            }
            assertEquals(Robot.GRID_SIZE, myRobot.row());}
            catch (IllegalMoveException e){
            fail();
            }
    }
    @Test
    public void moveOutRight() throws IllegalMoveException{
        try{
            myRobot.turn();
            System.out.println(myRobot.getDirection());
            for (int i = 0; i < Robot.GRID_SIZE ; i++) {
                myRobot.move();
                System.out.println(myRobot.column());
            }
            fail();
        }
        catch (IllegalMoveException e){
            assertEquals(Robot.GRID_SIZE, myRobot.column());
        }
    }

    @Test
    public void moveinleft() throws IllegalMoveException{
        myRobot.turn();
        myRobot.turn();
        myRobot.turn();
        System.out.println(myRobot.getDirection());
        int col = myRobot.column();
        System.out.println(col);
        int counter = 1;
        try{
        for (int i = 1; i <  col ; i++) {
            myRobot.move();
            System.out.println(myRobot.column());
            counter++;
        }
        assertEquals(counter, myRobot.column());}
        catch (IllegalMoveException e){
            fail();
        }
    }

    @Test
    public void moveOutleft() throws IllegalMoveException{
        try{
            myRobot.turn();
            myRobot.turn();
            myRobot.turn();
            System.out.println(myRobot.getDirection());
            for (int i = 0; i < Robot.GRID_SIZE ; i++) {
                myRobot.move();
                System.out.println(myRobot.column());
            }
            fail();
        }
        catch (IllegalMoveException e){
            assertEquals(1, myRobot.column());
        }
    }

    @Test
    public void moveinRight() throws IllegalMoveException{
        myRobot.turn();
        System.out.println(myRobot.getDirection());
        int col = myRobot.column();
        System.out.println(col);
        int counter = 1;
        try{
            for (int i = 1; i <  Robot.GRID_SIZE ; i++) {
                myRobot.move();
            }
            assertEquals(counter, myRobot.column());}
        catch (IllegalMoveException e){
            fail();
        }
    }

    @Test
    public void directionFacing() throws IllegalMoveException{
        Robot.Direction direction = myRobot.getDirection();
        int randomMovement = (int) (Math.random()*Robot.GRID_SIZE+1);
        try{
            for (int i = 0; i < randomMovement; i++) {
                myRobot.move();
            }
            assertEquals(direction, myRobot.getDirection());
        }
        catch (IllegalMoveException e){
            fail();
        }
    }

    @Test
    public void testingBacktrack(){
        for (int i = 0; i <5 ; i++) {
            myRobot.turn();
        }
        int size = myRobot.states.size();
        myRobot.backTrack();
        assertEquals(myRobot.states.size(),size-1 );
    }

    @Test
    public void testingCurrentState(){
        Robot.RobotState size = myRobot.states.get( myRobot.states.size()-1);
        assertEquals(myRobot.states.get(myRobot.states.size()-1), size );
    }
}
